from django.contrib import admin
from flash_card_app.models import Flash_card
# Register your models here.

@admin.register(Flash_card)
class Flash_card_admin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
