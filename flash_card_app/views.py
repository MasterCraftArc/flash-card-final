from django.shortcuts import render,get_object_or_404,redirect
from flash_card_app.models import Flash_card
from flash_card_app.forms import Flash_card_form
# Create your views here.
def show_flashcard(request):
    flashcard = get_object_or_404(Flash_card)
    context = {
        "flashcard_object": flashcard,
    }
    return render(request,"flash_card/base.html",context)

def flash_card_list(request):
    flashcard = Flash_card.objects.all()
    context = {
        "flash_card_list": flashcard,
    }
    return render(request,"flash_card/base.html",context)


def create_flashcard(request):
    if request.method == "POST":
        form = Flash_card_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_flashcard")
    else:
        form = Flash_card_form()

    context = {
        "form": form,
    }

    return render(request,"flash_card/create.html",context)
# Create your views here.
